#include <string>
#include <conio.h>
#include <iostream>
#include <time.h>
using namespace std;

int main()
{
	srand(time(NULL));
	int numbers[20];
	int lar1, lar2;
	for (int i = 0; i < 20; i++)
	{
		numbers[i] = rand() % 100 + 1;
		cout << numbers[i] << " ";
	}
	lar1 = numbers[0]; // lar1 = largest
	lar2 = numbers[1]; // lar2 = second largest 
	
	for (int i = 0; i < 20; i++) 
	{
		if (numbers[i] > lar1) // for largest
		{
			lar2 = lar1;
			lar1 = numbers[i];
		}
		else if (numbers[i] > lar2) // for second largest
		{
			lar2 = numbers[i];
		}
	}
	
	// duplicates
	for (int i = 0; i < 20; i++) // counter for each index [0-20]
	{
		int duplicates = 0;
		for (int x = 0; x < 20; x++) // counter for each indices' duplicates
		{
			if(i != x)
			{
				if (numbers[i] == numbers[x])
					duplicates++;
			}
		}
		if (duplicates > 0)
		{
			cout << endl << numbers[i] << " duplicates: " << duplicates;
		}
	}

	cout << endl << "Largest element: " << lar1 << endl << "Second Largest element: " << lar2;
	
	_getch();
	return 0;
}