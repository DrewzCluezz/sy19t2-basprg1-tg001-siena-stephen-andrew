#include <iostream>
#include <string>
#include <conio.h>
#include <time.h>

using namespace std;

int main()
{
	srand(time(NULL));
	int array[99];
	int missing;
	int randomNumb = rand() % 99 + 1;
	for (int x = 0; x < 99; x++) // assigning values
	{
		if (x + 1 != randomNumb)
		{
			array[x] = x + 1;
			cout << array[x] << " " ;
		}
	}
	for (int i = 0; i < 99; i++)// this loop is used to find the missing number
	{
		if (array[0] != 1)
		{
			missing = 1;
			break;
		}
		if (array[i] + 1 != array[i + 1])
		{
			missing = array[i] + 1;
			break;
		}
		
	}
	cout << endl << "missing: " << missing;	
	_getch();
	return 0;
}