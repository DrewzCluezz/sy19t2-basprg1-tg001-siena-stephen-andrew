#include <string>
#include <conio.h>
#include <iostream>

using namespace std;

int factorial(int fact, int num)
{
	for (int i = 1; i <=num; i++)
	{ 
		fact = fact * i;

		cout << i << " * ";
	}

	return fact;
}

int main()
{
	int num;
	int fact = 1;
	cout << " Input number: ";
	cin >> num;
	
	fact = factorial(fact, num);

	cout << "Factorial of " << num << " is " << fact;

	_getch();
	return 0;
}