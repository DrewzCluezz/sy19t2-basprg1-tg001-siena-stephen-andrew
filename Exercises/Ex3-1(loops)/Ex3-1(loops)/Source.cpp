#include <conio.h>
#include <string>
#include <iostream>

using namespace std;

int main()
{
	int values;
	int numberOfValues;
	float sum = 0;
	cout << "Input numbers to be computed: ";
	cin >> numberOfValues;
	for (int i = 1; i <= numberOfValues; i++)
	{
		cout << "Input Value " << i << ": ";
		cin >> values;
		sum = sum + values;
	}
	cout << "Sum: " << sum << endl;
	float avg = sum / numberOfValues;
	cout << "The Average is: " << avg;



	_getch();
	return 0;
}