#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

int main()
{

	int terms;
	cout << "Input number of terms: ";
	cin >> terms;
	for (int i = 0; i <= terms * 2; i++)
	{
		if (i % 2 > 0)
		{
			cout << i << "  ";
		}
	}

	_getch();
	return 0;
}