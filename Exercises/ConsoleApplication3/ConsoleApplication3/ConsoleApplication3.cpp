// ConsoleApplication3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <conio.h>
#include <iostream>
#include <string>

using namespace std;


int main()
{
	int x;
	int y;

	cout << "Input X value: ";
	cin >> x;
	cout << "Input y value: ";
	cin >> y;
	//1 = true
	//0 = false
	cout << x << ", " << y << endl;
	if (x == y)
	{
		cout << x << "==" << y << " = " << " 1 " << endl;
	}
	else
	{
		cout << x << "==" << y << " = " << " 0 " << endl;
	}

	if (x != y)
	{
		cout << x << "!=" << y << " = " << " 1 " << endl;
	}
	else
	{
		cout << x << "!=" << y << " = " << " 0 " << endl;
	}

	if (x > y)
	{
		cout << x << ">" << y << " = " << " 1 " << endl;
	}
	else
	{
		cout << x << ">" << y << " = " << " 0 " << endl;
	}
	
	if (x < y)
	{
		cout << x << "<" << y << " = " << " 1 " << endl;
	}
	else
	{
		cout << x << "<" << y << " = " << " 0 " << endl;
	}

	if (x >= y)
	{
		cout << x << ">=" << y << " = " << " 1 " << endl;
	}
	else
	{
		cout << x << ">=" << y << " = " << " 0 " << endl;
 	}

	if (x <= y)
	{
		cout << x << "<=" << y << " = " << " 1 " << endl;
	}
	else
	{
		cout << x << "<=" << y << " = " << " 0 " << endl;
	}
	_getch();
	return 0;
	
}
