// ConsoleApplication4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <conio.h>
#include <time.h>
#include <iostream>
#include < string>

using namespace std;


int main()
{
	int hp;

	cout << "Input HP: ";
	cin >> hp;
	if (hp == 100)
	{
		cout << "Hero has Full Health" << endl;
	}
	else if (hp >= 50)
	{
		cout << "Hero is Healthy " << endl;
	}
	else if (hp >= 20 && hp < 50)
	{
		cout << "Warning! Hero Low Health " << endl;
	}
	else if (hp >= 1 && hp <= 20)
	{
		cout << "Hero is in Critical Health" << endl;
	}
	else // if hp is 0
	{
		cout << " Hero is DEAD" << endl;
	}

	_getch();
    return 0;
}

