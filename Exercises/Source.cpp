
#include <conio.h>
#include <string>
#include <iostream>
using namespace std;

int main()
{
	int startValue;
	int numberOfTerms;
	int sum = 0;

	cout << "Input starting value: ";
	cin >> startValue;

	cout << "Input number of terms: ";
	cin >> numberOfTerms;

	for (int i = 1; i <= numberOfTerms; i++)
	{
		//cout << numberOfTerms;
		cout << startValue << " + ";
		sum += startValue;
		startValue += 1;
	}
	cout << " = " << sum;
	_getch();
	return 0;
}