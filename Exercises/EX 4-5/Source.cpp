#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

int main()
{
	string input;
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll",
					   "RedPotion", "RedPotion", "Elixir" };
	
	int count = 0;

	cout << " Items: ";
	cin >> input;


	for (int i = 0; i <= 8; i++)
	{
		if (input == items[i])
			count++;
	}

	if (count == 0)
		cout << input << " does not exist" << endl;
	else
		cout << "You have " << input << " " << count << " times" << endl;
			

	_getch();
	return 0;
}
