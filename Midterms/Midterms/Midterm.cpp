#include <string>
#include <conio.h>
#include <iostream>
#include <time.h>
using namespace std;

int main()
{
	int heroHP;
	int aiHP;
	int minDamage;
	int maxDamage;
	char move;
	int AiMove;

	int aiDamageDone = 0;
	int heroDamageDone = 0;

	cout << "Hero VS Opponent" << endl;

	cout << "Hero HP: ";
	cin >> heroHP;

	cout << "AI HP: ";
	cin >> aiHP;

	cout << "Input Minimum Damage: ";
	cin >> minDamage;

	cout << "Input Maximum Damage: ";
	cin >> maxDamage;
	cout << endl;

	while (heroHP > 0 && aiHP > 0)
	{
		cout << "Hero Hp: " << heroHP << endl;
		cout << "Ai Hp: " << aiHP << endl << endl;
		cout << "Make a move: A=Attack, D=Defend, W=Wild Attack" << endl;
		cin >> move;
		AiMove = rand() % 3 + 1;
		if (move == 'A' || move == 'a')
		{
			if (AiMove == 1)//Attack on Attack = 100% damage to both
			{
				cout << "Opponent has decided to use Attack" << endl;
				aiDamageDone = rand() % (maxDamage - minDamage + 1) + minDamage;
				heroHP = heroHP - aiDamageDone;

				heroDamageDone = rand() % (maxDamage - minDamage + 1) + minDamage;
				aiHP = aiHP - heroDamageDone;
			}

			if (AiMove == 2)//attack on defend = enemy block 50% of incoming damage
			{
				cout << "Opponent has decided to use Defend" << endl;
				heroDamageDone = rand() % (maxDamage - minDamage + 1) + minDamage;
				heroDamageDone = heroDamageDone / 2;
				aiHP = aiHP - heroDamageDone;
			}
			if (AiMove == 3)//attack on Wild Attack = deal 100% && Recieve 200%
			{
				cout << "Opponent has decided to use Wild Attack" << endl;
				aiDamageDone = rand() % (maxDamage - minDamage + 1) + minDamage;
				aiDamageDone = aiDamageDone * 2;
				heroHP = heroHP - aiDamageDone;
				heroDamageDone = rand() % (maxDamage - minDamage + 1) + minDamage;
				aiHP = aiHP - heroDamageDone;
			}
		}
		else if (move == 'D' || move == 'd') // Hero Defend
		{
			if (AiMove == 1)// user blocks 50% of incoming damage
			{
				cout << "Opponent has decided to use Attack" << endl;
				aiDamageDone = rand() % (maxDamage - minDamage + 1) + minDamage;
				aiDamageDone = aiDamageDone / 2;
				heroHP = heroHP - aiDamageDone;
			}

			if (AiMove == 2)// defend on defend, nothing happens
			{
				cout << "Opponent has decided to use Defend" << endl;
			}

			if (AiMove == 3)//defend on Wild Attack,Opponent�s attack will miss and you counter the opponent with 200 % damage.
			{
				cout << "Opponent has decided to use Wild Attack" << endl;
				cout << "Opponent missed his attack" << endl;
				heroDamageDone = rand() % (maxDamage - minDamage + 1) + minDamage;
				heroDamageDone = heroDamageDone * 2;
				aiHP = aiHP - heroDamageDone;
			}
		}
		else if (move == 'W' || move == 'w') // Hero Wild Attack
		{
			if (AiMove == 1) //Wild Attack on  Attack, user deals 200%, AI deals 100%
			{
				cout << "Opponent decided to use attack" << endl;
				heroDamageDone = rand() % (maxDamage - minDamage + 1) + minDamage;
				heroDamageDone = heroDamageDone * 2;
				aiHP = aiHP - heroDamageDone;
				aiDamageDone = rand() % (maxDamage - minDamage + 1) + minDamage;
				heroHP = heroHP - aiDamageDone;
			}

			if (AiMove == 2)// wild attack on defend, user misses, AI counters with 200%
			{
				cout << "Hero Missed wild attack" << endl;
				aiDamageDone = rand() % (maxDamage - minDamage + 1) + minDamage;
				aiDamageDone = aiDamageDone * 2;
				heroHP = heroHP - aiDamageDone;
			}

			if (AiMove == 3) // Wild Attack on  Wild Attack, user n Ai deal 200%
			{
				cout << "Opponent decided to use Wild Attack" << endl;
				aiDamageDone = rand() % (maxDamage - minDamage + 1) + minDamage;
				aiDamageDone = aiDamageDone * 2;
				heroHP = heroHP - aiDamageDone;

				heroDamageDone = rand() % (maxDamage - minDamage + 1) + minDamage;
				heroDamageDone = heroDamageDone * 2;
				aiHP = aiHP - heroDamageDone;
			}
		}

		cout << "Ai dealt: " << aiDamageDone << endl;
		cout << "Hero dealt: " << heroDamageDone << " damage" << endl << endl;

		cout << "Remaining Hero Hp: " << heroHP << endl;
		cout << "Remaining Ai Hp: " << aiHP << endl;
		system("pause");
		system("cls");
	}
	if (heroHP <= 0)
		cout << "You Died";

	else if (aiHP <= 0)
		cout << "Victorious";



	_getch();
	return 0;
}